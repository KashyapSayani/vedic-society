package com.kashyap.vedicsociety;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kashyap.vedicsociety.model.VedicSocietyModel;

import java.util.ArrayList;

public class VedicSocietyListAdapter extends RecyclerView.Adapter<VedicSocietyListAdapter.VedicSocietyViewHolder> {

    ArrayList<VedicSocietyModel> vedicSocietyModelList;
    ArrayList<VedicSocietyModel> tempVedicSocietyList;
    OnClickListener onClickListner;

    public VedicSocietyListAdapter(ArrayList<VedicSocietyModel> vedicSocietyModelList, OnClickListener onClickListener) {
        this.vedicSocietyModelList = vedicSocietyModelList;
        tempVedicSocietyList = new ArrayList<>(vedicSocietyModelList);
        this.onClickListner = onClickListener;
    }

    @NonNull
    @Override
    public VedicSocietyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.vedic_society_list_layout, parent, false);
        return new VedicSocietyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VedicSocietyViewHolder holder, final int position) {
        holder.tvWord.setText(vedicSocietyModelList.get(position).getWord());
        holder.tvNagari.setText(vedicSocietyModelList.get(position).getNagari());
        /*holder.tvDescription.setText(vedicSocietyModelList.get(position).getDescription());
        holder.tvCategory.setText(vedicSocietyModelList.get(position).getCategory());*/

        if (position % 2 == 0) {
            holder.linearLayout.setBackgroundResource(R.drawable.backgroundveditsocietylist1);
        } else {
            holder.linearLayout.setBackgroundResource(R.drawable.backgroundveditsocietylist2);
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListner != null) {
                    onClickListner.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return vedicSocietyModelList.size();
    }

    public interface OnClickListener {
        void onItemClick(int position);
    }

    public class VedicSocietyViewHolder extends RecyclerView.ViewHolder {

        TextView tvWord;
        TextView tvNagari;
        /*TextView tvDescription;
        TextView tvCategory;*/
        LinearLayout linearLayout;

        public VedicSocietyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvWord = itemView.findViewById(R.id.tvWord);
            tvNagari = itemView.findViewById(R.id.tvNagari);
            /*tvDescription = itemView.findViewById(R.id.tvDescription);
            tvCategory = itemView.findViewById(R.id.tvCategory);*/
            linearLayout = itemView.findViewById(R.id.linearLayout);
        }
    }
}
