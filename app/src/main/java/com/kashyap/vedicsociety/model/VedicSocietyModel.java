package com.kashyap.vedicsociety.model;

import org.json.JSONObject;

import java.io.Serializable;

public class VedicSocietyModel extends JSONObject implements Serializable {
    String Word;
    String Nagari;
    String Description;
    String Category;

    public String getWord() {
        return Word;
    }

    public void setWord(String word) {
        Word = word;
    }

    public String getNagari() {
        return Nagari;
    }

    public void setNagari(String nagari) {
        Nagari = nagari;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    @Override
    public String toString() {
        return "VedicSocietyModel{" +
                "Word='" + Word + '\'' +
                ", Nagari='" + Nagari + '\'' +
                ", Description='" + Description + '\'' +
                ", Category='" + Category + '\'' +
                '}';
    }
}
