package com.kashyap.vedicsociety;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.kashyap.vedicsociety.model.VedicSocietyModel;

public class VedicWordsDetails extends BaseActivity {

    TextView tvWord;
    TextView tvNagari;
    TextView tvDescription;
    TextView tvCategory;
    VedicSocietyModel vedicSocietyModel = new VedicSocietyModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vedic_words_details);
        initViewRefrence();
        bindValues();
        setUpActionBar(getString(R.string.app_name), false);
    }

    void initViewRefrence() {

        tvWord = findViewById(R.id.tvWord);
        tvNagari = findViewById(R.id.tvNagari);
        tvDescription = findViewById(R.id.tvDescription);
        tvCategory = findViewById(R.id.tvCategory);
    }

    void bindValues() {
        if (getIntent().hasExtra("Vedic Word Object")) {
            vedicSocietyModel = (VedicSocietyModel) getIntent().getSerializableExtra("Vedic Word Object");
            tvWord.setText(vedicSocietyModel.getWord());
            tvNagari.setText(vedicSocietyModel.getNagari());
            tvDescription.setText(vedicSocietyModel.getDescription());
            tvCategory.setText(vedicSocietyModel.getCategory());
            Log.d("User:::::", vedicSocietyModel.toString());
        } else {
            Log.d("User:::::", "No Data Found.");
        }
    }
}