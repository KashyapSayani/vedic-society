package com.kashyap.vedicsociety.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kashyap.vedicsociety.model.VedicSocietyModel;

import java.util.ArrayList;

public class VedicSocietyTbl extends MyDatabase {

    public static final String TABLE_NAME = "VedicSocietyTbl";
    public static final String WORD = "Word";
    public static final String NAGARI = "Nagari";
    public static final String DESCRIPTION = "Description";
    public static final String CATEGORY = "Category";
    ArrayList<VedicSocietyModel> vedicSocietyModelList = new ArrayList<>();

    public VedicSocietyTbl(Context context) {
        super(context);
    }

    public VedicSocietyModel getCreatedModelUsingCursor(Cursor cursor) {

        VedicSocietyModel vedicSocietyModel = new VedicSocietyModel();

        vedicSocietyModel.setWord(cursor.getString(cursor.getColumnIndex(WORD)));
        vedicSocietyModel.setNagari(cursor.getString(cursor.getColumnIndex(NAGARI)));
        vedicSocietyModel.setDescription(cursor.getString(cursor.getColumnIndex(DESCRIPTION)));
        vedicSocietyModel.setCategory(cursor.getString(cursor.getColumnIndex(CATEGORY)));
        return vedicSocietyModel;
    }

    public long insertData(String Word, String Nagari, String Description, String Category) {

        // Gets the data repository in write mode

        SQLiteDatabase db = getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues cv = new ContentValues();

        cv.put(WORD, Word);
        cv.put(NAGARI, Nagari);
        cv.put(DESCRIPTION, Description);
        cv.put(CATEGORY, Category);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(TABLE_NAME, null, cv);

        db.close();

        return newRowId;
    }

    public ArrayList<VedicSocietyModel> getVedicSocietyList() {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            vedicSocietyModelList.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();

        return vedicSocietyModelList;
    }
}
