package com.kashyap.vedicsociety;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.kashyap.vedicsociety.database.VedicSocietyTbl;
import com.kashyap.vedicsociety.model.VedicSocietyModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    RecyclerView rcvUserList;
    ArrayList<VedicSocietyModel> vedicSocietyModelList = new ArrayList<>();
    ArrayList<VedicSocietyModel> tempVedicSocietyList = new ArrayList<>();
    EditText etVedicWordSearch;
    VedicSocietyListAdapter vedicSocietyListAdapter;
    TextView tvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*getResponce();*/
        initViewRefrence();
        setUpActionBar(getString(R.string.app_name), false);
        getDataAndSetAdapter();
        searchUser();
    }

    public void getResponce() {
        String URL = "https://sheetlabs.com/IND/vs";

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        JSONArray jsonArray = response;

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object1 = null;
                            try {

                                object1 = jsonArray.getJSONObject(i);


                                long lastInsertedId = new VedicSocietyTbl(getApplicationContext()).insertData(object1.getString("word"), object1.getString("nagari"),
                                        object1.getString("description"), object1.getString("category"));

                                if (lastInsertedId > 0) {
                                    Log.d("Data:::::","Data Inserted Sucessfully");
                                } else {
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Rest Responce", error.toString());
                    }
                }
        );
        requestQueue.add(jsonArrayRequest);
    }

    public ArrayList<VedicSocietyModel> fetchDataFromDb() {
        ArrayList<VedicSocietyModel> vedicSocietyModels = new VedicSocietyTbl(getApplicationContext()).getVedicSocietyList();
        return vedicSocietyModels;
    }

    public void initViewRefrence() {
        rcvUserList = findViewById(R.id.rcvVedicWordList);
        etVedicWordSearch = findViewById(R.id.etVedicWordSearch);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
    }

    public void getDataAndSetAdapter() {
        vedicSocietyModelList = fetchDataFromDb();
        tempVedicSocietyList.addAll(vedicSocietyModelList);
        rcvUserList.setLayoutManager(new LinearLayoutManager(this));
        vedicSocietyListAdapter = new VedicSocietyListAdapter(tempVedicSocietyList, new VedicSocietyListAdapter.OnClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent vedicWordsDetailsIntent = new Intent(MainActivity.this, VedicWordsDetails.class);
                vedicWordsDetailsIntent.putExtra("Vedic Word Object", tempVedicSocietyList.get(position));
                startActivity(vedicWordsDetailsIntent);
            }
        });
        rcvUserList.setAdapter(vedicSocietyListAdapter);
    }

    void searchUser() {
        etVedicWordSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                tempVedicSocietyList.clear();
                if (s.toString().length() > 0) {

                    for (int i = 0; i < vedicSocietyModelList.size(); i++) {

                        s = s.toString().trim().toLowerCase();


                        if (vedicSocietyModelList.get(i).getWord().toLowerCase().contains(s) ||
                                vedicSocietyModelList.get(i).getCategory().toLowerCase().contains(s)) {
                            tempVedicSocietyList.add(vedicSocietyModelList.get(i));
                        }
                    }
                }

                if (s.toString().length() == 0 && tempVedicSocietyList.size() == 0) {
                    tempVedicSocietyList.addAll(vedicSocietyModelList);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void checkAndVisibleView() {
        if (vedicSocietyListAdapter.getItemCount() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else if (vedicSocietyListAdapter.getItemCount() <= 0) {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

    void resetAdapter() {

        checkAndVisibleView();

        if (vedicSocietyListAdapter != null) {
            vedicSocietyListAdapter.notifyDataSetChanged();
        }
    }
}